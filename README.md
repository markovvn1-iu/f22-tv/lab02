<div align="center">
  <img src=".gitlab/logo.svg" height="80px"/><br/>
  <h1>Lab 2</h1>
  <p>KVM</a></p>
</div>

### Looking for a main execution cycle

1. Download the source code:

   ```bash
   wget https://git.ti.com/cgit/ti-linux-kernel/ti-linux-kernel/snapshot/ti-linux-kernel-3.18.53.tar.gz
   ```

2. Unpack the code:

   ```bash
   tar -xf ti-linux-kernel-3.18.53.tar.gz && cd ti-linux-kernel-3.18.53
   ```

3. Find all files containing the word `guest`:

   ```bash
   grep -R guest | awk -F":" '{print $1}' | uniq
   ```

   Output:

   ```
   include/kvm/arm_vgic.h
   ...
   virt/kvm/ioapic.c
   virt/kvm/iommu.c
   ...
   arch/x86/kernel/kvm.c
   ...
   arch/x86/kvm/pmu.c
   arch/x86/kvm/cpuid.c
   arch/x86/kvm/vmx.c
   arch/x86/kvm/x86.h
   ...
   ```

   Seems like the folders we are looking for are `arch/x86/kvm/`,  `arch/x86/kernel/`, `virt/kvm/`, `include/kvm/`.

4. We are looking for source code, so, probably we are interested in `arch/x86/kvm/` folder. Go to folder `arch/x86/kvm` and continue looking for loops:

   ```bash
   grep -R while
   ```

   There are only ~20 while loops. So, we can check all them manually. After 2 minutes of work I find out that loop `while (r > 0) {`  (line 6393 in `arch/x86/kvm/x86.c`) is the one I am looking for. The function that contains this cycle is called `__vcpu_run`. This function is called from the function ` kvm_arch_vcpu_ioctl_run` (line 6534 in `arch/x86/kvm/x86.c`). This function has a documentation that says:

   ```
   /**
    * kvm_arch_vcpu_ioctl_run - the main VCPU run function to execute guest code
    * @vcpu:	The VCPU pointer
    * @run:	The kvm_run structure pointer used for userspace state exchange
    *
    * This function is called through the VCPU_RUN ioctl called from user space. It
    * will execute VM code in a loop until the time slice for the process is used
    * or some emulation is needed from user space in which case the function will
    * return with return value 0 and with the kvm_run structure filled in with the
    * required data for the requested emulation.
    */
   ```

​		This is the main loop for VCPU. But TA says it is not we are looking for (it is loop for VCPU, but we need the loop of the VM). So, continue searching for the main loop...

The function `kvm_arch_vcpu_ioctl_run` is called from function ` kvm_vcpu_ioctl` (line 2075 in`virt/kvm/kvm_main.c`). And this function is called from function `kvm_vcpu_ioctl` (line 2042 in `virt/kvm/kvm_main.c`). This function has switch-case for processing VM event:

```c++
case KVM_RUN:
	r = -EINVAL;
	if (arg)
		goto out;
	r = kvm_arch_vcpu_ioctl_run(vcpu, vcpu->run);  // do kvm work
	trace_kvm_userspace_exit(vcpu->run->exit_reason, r);  // check exit code
	break;
case KVM_GET_REGS:
case KVM_SET_REGS:
...
case KVM_TRANSLATE:
...
case KVM_GET_FPU:
case KVM_SET_FPU:
```

So, probably it is the main loop of the KVM.

### Looking for a common place for handling all page faults in VT-x

1. First, let's get to the bottom of what to look for in the first place. Open [Wikipedia](https://en.wikipedia.org/wiki/X86_virtualization) and see:

   > The CPU flag for VT-x capability is "vmx"

   > "VMX" stands for Virtual Machine Extensions

   So, we are looking for something called vmx.

2. From the step 3 we can see that there is a file called `arch/x86/kvm/vmx.c`. Probably it is what we are looking for. Let's search `page_fault` in this file:

   ```c++
   static inline bool is_page_fault(u32 intr_info)
   {
   	return (intr_info & (INTR_INFO_INTR_TYPE_MASK | INTR_INFO_VECTOR_MASK |
   			     INTR_INFO_VALID_MASK)) ==
   		(INTR_TYPE_HARD_EXCEPTION | PF_VECTOR | INTR_INFO_VALID_MASK);
   }
   ```

   Probably this function check if there is a page fault

3. Let's look the this function is called from the function ` handle_exception` (line 4877 in file `arch/x86/kvm/vmx.c`). In this function there is a code snippet we are looking for:

   ```C++
   static int handle_exception(struct kvm_vcpu *vcpu)
   {
       ...
       if (is_page_fault(intr_info)) {
           /* EPT won't cause page fault directly */
           BUG_ON(enable_ept);
           cr2 = vmcs_readl(EXIT_QUALIFICATION);
           trace_kvm_page_fault(cr2, error_code);
   
           if (kvm_event_needs_reinjection(vcpu))
               kvm_mmu_unprotect_page_virt(vcpu, cr2);
           return kvm_mmu_page_fault(vcpu, cr2, error_code, NULL, 0);
       }
   ...
   }
   ```

​		I think this part is responsible for handling all page faults in VT-x.

### Looking for entry and exit point of the guest

1. Looking for something related to guest and enter:

   ```bash
   grep -R guest | grep enter
   ```

   Output:

   ```
   ...
   arch/x86/kvm/x86.c:	kvm_guest_enter();
   ...
   ```

2. This function is called from function `vcpu_enter_guest` (line 6167 in `arch/x86/kvm/x86.c`):

   ```C++
   static int vcpu_enter_guest(struct kvm_vcpu *vcpu)
   {
       ...  // prepare something on host
       kvm_guest_enter();  // enter to the guest
       ...  // do mothing in guest
       kvm_x86_ops->run(vcpu);  // run some process in the guest
       ... //  do something else in guest
       kvm_guest_exit();  // exit from the guest
       ...  // do something more on host
   }
   ```

   So... Maybe this is what we are looking for.

### Results

| Task                                                | Location                           |
| --------------------------------------------------- | ---------------------------------- |
| A function where the looping is done                | line 2042 in `virt/kvm/kvm_main.c` |
| The code line after that guest starts               | line 6293 in `arch/x86/kvm/x86.c`  |
| The code line after that guest exits                | line 6353 in `arch/x86/kvm/x86.c`  |
| The code line where VMM starts after guest return   | line 2229 in `virt/kvm/kvm_main.c` |
| A common place for handling all page faults in VT-x | line 4926 in `arch/x86/kvm/vmx.c`  |

